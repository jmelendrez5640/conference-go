import json
import requests



def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {'Authorization':'EofO39xmq94uKRU5gAmxU8fljpRz4DFwxbuOrlLfFrqduipj2Lmg6Cwi'}
    query = f"{city}, {state}"
    response = requests.get(url, headers=headers, params={'query': query, 'orientation': 'landscape', 'per_page': 5})
    photo = json.loads(response.content)
    get_the_photo = {
        "photo_url": photo['photos'][0]['src']['original']
    }

    return get_the_photo


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{366-2}&limit=1&appid=b2b8568f2474cad114bec2abc721f569"
    response = requests.get(url)
    long_lat = json.loads(response.content)
    lon = long_lat[0]['lon']
    lat = long_lat[0]['lat']

    url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=b2b8568f2474cad114bec2abc721f569"
    response2 = requests.get(url2)
    weather = json.loads(response2.content)
    weather_result = {
        "temp": weather["main"]["temp"],
        "description": weather["weather"][0]["description"]
    }
    return weather_result
